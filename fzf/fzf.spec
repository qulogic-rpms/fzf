# If any of the following macros should be set otherwise,
# you can wrap any of them with the following conditions:
# - %%if 0%%{centos} == 7
# - %%if 0%%{?rhel} == 7
# - %%if 0%%{?fedora} == 23
# Or just test for particular distribution:
# - %%if 0%%{centos}
# - %%if 0%%{?rhel}
# - %%if 0%%{?fedora}
#
# Be aware, on centos, both %%rhel and %%centos are set. If you want to test
# rhel specific macros, you can use %%if 0%%{?rhel} && 0%%{?centos} == 0 condition.
# (Don't forget to replace double percentage symbol with single one in order to apply a condition)

# Generate devel rpm
%global with_devel 0
# Build project from bundled dependencies
%global with_bundled 0
# Build with debug info rpm
%global with_debug 1
# Run tests in check section
%global with_check 1
# Generate unit-test rpm
%global with_unit_test 0

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif


%global provider        github
%global provider_tld    com
%global project         junegunn
%global repo            fzf
# https://github.com/junegunn/fzf
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}
%global commit          ecb6b234cc130c760f792f95ce59b413be4cb5c4
%global shortcommit     %(c=%{commit}; echo ${c:0:7})
%global commitdate      20170801

Name:           fzf
Version:        0.16.11
Release:        6%{?dist}
Summary:        A command-line fuzzy finder written in Go
License:        MIT
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{version}/%{repo}-%{version}.tar.gz
Source1:        README.Fedora

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 aarch64 %{arm}}
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}

BuildRequires: golang(github.com/gdamore/tcell)
BuildRequires: golang(github.com/gdamore/tcell/encoding)
BuildRequires: golang(github.com/mattn/go-isatty)
BuildRequires: golang(github.com/mattn/go-runewidth)
BuildRequires: golang(github.com/mattn/go-shellwords)
BuildRequires: golang(golang.org/x/crypto/ssh/terminal)


%description
fzf is a general-purpose command-line fuzzy finder.

It's an interactive Unix filter for command-line that can be used with any
list; files, command history, processes, hostnames, bookmarks, git commits,
etc.

%if 0%{?with_devel}
%package -n golang-%{provider}-%{project}-%{repo}-devel
Summary:       %{summary}
BuildArch:     noarch

Requires:      golang(github.com/gdamore/tcell)
Requires:      golang(github.com/gdamore/tcell/encoding)
Requires:      golang(github.com/mattn/go-isatty)
Requires:      golang(github.com/mattn/go-runewidth)
Requires:      golang(github.com/mattn/go-shellwords)
Requires:      golang(golang.org/x/crypto/ssh/terminal)

Provides:      golang(%{import_path}/src) = %{version}-%{release}
Provides:      golang(%{import_path}/src/algo) = %{version}-%{release}
Provides:      golang(%{import_path}/src/tui) = %{version}-%{release}
Provides:      golang(%{import_path}/src/util) = %{version}-%{release}

%description -n golang-%{provider}-%{project}-%{repo}-devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%package -n golang-%{provider}-%{project}-%{repo}-unit-test-devel
Summary:         Unit tests for %{name} package
%if 0%{?with_check}
#Here comes all BuildRequires: PACKAGE the unit tests
#in %%check section need for running
%endif

# test subpackage tests code from devel subpackage
Requires:        golang-%{provider}-%{project}-%{repo}-devel = %{version}-%{release}

%if 0%{?with_check} && ! 0%{?with_bundled}
%endif


%description -n golang-%{provider}-%{project}-%{repo}-unit-test-devel
%{summary}

This package contains unit tests for project
providing packages with %{import_path} prefix.
%endif

%prep
%setup -q -n %{repo}-%{version}
cp %{SOURCE1} .


%build
mkdir -p src/%{provider}.%{provider_tld}/%{project}/
ln -s $(pwd) src/%{provider}.%{provider_tld}/%{project}/%{repo}
export GOPATH=$(pwd):%{gopath}

%gobuild -o bin/fzf %{import_path}

# Cleanup interpreters
sed -i -e '/^#!\//, 1d' shell/completion.*
sed -i -e '1d;2i#!/bin/bash' bin/fzf-tmux


%install
install -d -p %{buildroot}%{_bindir}
install -Dpm0755 bin/fzf %{buildroot}%{_bindir}/
install -Dpm0755 bin/fzf-tmux %{buildroot}%{_bindir}/
install -d -p %{buildroot}%{_mandir}/man1
install -Dpm0644 man/man1/*.1 %{buildroot}%{_mandir}/man1/

install -d %{buildroot}%{_datadir}/fzf

# Install vim plugin
install -d %{buildroot}%{_datadir}/vim/vimfiles/plugin
install -Dpm0644 plugin/fzf.vim %{buildroot}%{_datadir}/vim/vimfiles/plugin/

# Install shell completion
install -d %{buildroot}%{_sysconfdir}/bash_completion.d
install -Dpm0644 shell/completion.bash %{buildroot}%{_sysconfdir}/bash_completion.d/fzf
install -d %{buildroot}%{_datadir}/zsh/site-functions
install -Dpm0644 shell/completion.zsh %{buildroot}%{_datadir}/zsh/site-functions/fzf

# Install shell key bindings (not enabled)
install -d %{buildroot}%{_datadir}/fzf/shell
install -Dpm0644 shell/key-bindings.* %{buildroot}%{_datadir}/fzf/shell/

# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
echo "%%dir %%{gopath}/src/%%{import_path}/." >> devel.file-list
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . \( -iname "*.go" -or -iname "*.s" \) \! -iname "*_test.go") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test-devel.file-list
for file in $(find . -iname "*_test.go") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test-devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif

%check
%if 0%{?with_check} && 0%{?with_unit_test} && 0%{?with_devel}
%if ! 0%{?with_bundled}
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%else
# No dependency directories so far

export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%endif

%if ! 0%{?gotest:1}
%global gotest go test
%endif

%gotest %{import_path}/src
%gotest %{import_path}/src/algo
%gotest %{import_path}/src/tui
%gotest %{import_path}/src/util
%endif

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md README-VIM.md CHANGELOG.md README.Fedora
%{_bindir}/fzf
%{_bindir}/fzf-tmux
%{_mandir}/man1/fzf.1*
%{_mandir}/man1/fzf-tmux.1*
%dir %{_datadir}/fzf
%{_datadir}/fzf/shell
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/fzf
%dir %{_datadir}/vim/vimfiles/plugin
%{_datadir}/vim/vimfiles/plugin/fzf.vim
%{_sysconfdir}/bash_completion.d/fzf

%if 0%{?with_devel}
%files -n golang-%{provider}-%{project}-%{repo}-devel -f devel.file-list
%license LICENSE
%doc README.md CHANGELOG.md
%dir %{gopath}/src/%{provider}.%{provider_tld}/%{project}
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files -n golang-%{provider}-%{project}-%{repo}-unit-test-devel -f unit-test-devel.file-list
%license LICENSE
%doc README.md CHANGELOG.md
%endif

%changelog
* Thu Aug 24 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.16.11-6
- Turn off source and unit testing packages.

* Wed Aug 23 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.16.11-5
- Enable vim plugin by default.
- Fix directory ownership on plugins.

* Mon Aug 21 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.16.11-4
- Restore dist tag.

* Sun Aug 20 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.16.11-3
- Fix source URL.
- Enable debuginfo subpackage.
- Correct interpreters in shebangs.

* Sun Aug 20 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.16.11-2
- Add fzf binary to package.
- Add Fedora-specific readme about optional things.
- Install shell completions.
- Install (disabled) vim plugin also.
- Install (disabled) shell key bindings.

* Fri Aug 18 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.16.11-1
- Initial package for Fedora
